/*
    Code3: Key Pair

    Company: Zoho, Flipkart, Morgan Stanley, Accolite, Amazon, Microsoft,
             FactSet, Hike, Adobe, Google, Wipro, SAP Labs, CarWale

    Platform: GFG
    
    Description:
    Given an array Arr of N positive integers and another number X. Determine
    whether or not there exist two elements in Arr whose sum is exactly X.
    
    Example 1:
    Input:
        N = 6, X = 16
        Arr[] = {1, 4, 45, 6, 10, 8}
    Output: Yes
    Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
    
    Example 2:
    Input:
        N = 5, X = 10
        Arr[] = {1, 2, 4, 3, 6}
    Output: Yes
    Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1 ≤ N ≤ 105
        1 ≤ Arr[i] ≤ 105
 */

import java.util.*;

class Solution {

    /*
    BruteForce Approach :
    static boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        
        for(int i = 0; i < n; i++) {
            int sum = 0;

            for(int j = 0; j < n; j++) {    
                if(arr[i] + arr[j] == x && i != j)
                    return true;
            }
        }
        return false;
    }
     */

    static boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        
        HashSet<Integer> hs = new HashSet<>();

        for(int i = 0; i < n; i++) {

            if(hs.contains(x-arr[i]))
                return true;
            else
                hs.add(arr[i]);
        }
        return false;
    }

    public static void main(String[] args) {
        
        int Arr[] = {1, 4, 45, 6, 10, 8};

        System.out.println(hasArrayTwoCandidates(Arr, Arr.length, 16));
    }
}