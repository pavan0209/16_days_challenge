/*
    Code 1: Majority Element
    
    Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
    Platform : Leetcode - 169, GFG
    Fraz’s & striver’s SDE sheet.
    
    Description

    Given an array nums of size n, return the majority element.
    The majority element is the element that appears more than [n / 2⌋ times. 
    You may assume that the majority element always exists in the array.
    
    Example 1:
    Input: nums = [3,2,3]
    Output: 3
    
    Example 2:
    Input: nums = [2,2,1,1,1,2,2]
    Output: 2
    
    Constraints:
        n == nums.length
        1 <= n <= 5 * 104
        -109 <= nums[i] <= 109
 */

class Solution {

    static int majorityElement(int a[], int size) {

        java.util.Arrays.sort(a);
        int val = size / 2;
        int cnt = 0;

        for (int i = 1; i < size; i++) {

            cnt++;
            if (a[i] != a[i - 1]) {
                if (cnt > val)
                    return a[i - 1];

                cnt = 0;
            }
        }

        return (cnt >= val) ? a[size-1] : -1;
    }

    public static void main(String[] args) {

        int a[] = { 3, 1, 3, 3, 2 };

        System.out.println(majorityElement(a, a.length));
    }
}